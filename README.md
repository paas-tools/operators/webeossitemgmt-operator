This Ansible operator handles user-facing CRD for webeos:

`UserProvidedDirectory` and `CentrallyManagedDirectory`

It uses OLM's `ClusterServiceVersion` to provide a UI when site owners
create these resources.

## Iterating on changes to Ansible source

SDK has [instructions to run locally](https://github.com/operator-framework/operator-sdk/blob/ff657078952b44bf46fbfd9bf30ed84b500f1973/doc/ansible/dev/developer_guide.md#testing-an-ansible-operator-locally).

I had to install the following in a `centos:8` container (make sure to mount the kubeconfig, e.g. `docker run -it --rm -w /project -v 'C:\git\webeossitemgmt-operator:/project' -v C:\git\okd4-install\auth\kubeconfig.webeosproto:/root/.kube/config centos:8`):

```bash
# get current version from Dockerfile
RELEASE_VERSION=v0.19.2
curl -LO https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu
chmod +x operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu &&  mkdir -p /usr/local/bin/ &&  mv --force operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu /usr/local/bin/operator-sdk
dnf install -y python2-pip python2-devel gcc
pip-2 install ansible ansible-runner ansible-runner-http openshift kubernetes jmespath
ansible-galaxy collection install -r requirements.yml
# run locally, watch all namespaces
operator-sdk run --local --watch-namespace ""
```

For an in-cluster deployment:

First deploy the operator using the standard method that allows iterating on changes
to a component of okd4-install (see doc/components in okd4-install project):
delete Argo-CD `Application` and deploy with Helm. I deployed
the operator to using the `latest` tag and was rebuilding it as follows:

Iterating by building image locally is very fast because only the Ansible YAML files need to change
so one can do as follows:

```
# docker build very fast since only the Ansible YAML is added
docker build . --file build/Dockerfile -t gitlab-registry.cern.ch/paas-tools/operators/webeossitemgmt-operator
docker push gitlab-registry.cern.ch/paas-tools/operators/webeossitemgmt-operator
# restart operator
oc delete pod -l name=webeossitemgmt-operator -n webeos
```

## Iterating on changes to the Helm chart (e.g. changes to CRD, CSV...)

Disable ArgoCD and deploy the local helm chart directly to the `webeos` namespace of a dev cluster.
However _Helm must not be used to manage the CRDs_:

```bash
# stop the argicd application (only once)
oc get application webeossitemgmt-operator -n openshift-cern-argocd -o json | jq -r .spec.source.helm.values > /tmp/values.yaml
oc delete application webeossitemgmt-operator
# Run the following for each iteration. Don't manage the CRDs with Helm!
helm upgrade --install webeossitemgmt-operator deploy/ --values /tmp/values.yaml --namespace webeos --skip-crds
# use `oc replace` to update the CRDs with each iteration
oc replace -f deploy/crds
```

We need to do like this because `oc delete application` will try to delete the CRD, but it will stay in deleting state
since the webeossitemgmt operator is also stopped and cannot run the finalizers. Helm then refuses to recreate
the CRD. In addition, Helm only deploys the CRD on installation; later `helm upgrade` won't touch the CRD
so `oc replace` is required to deploy changes.

## Using OLM's CSV to get a UI for our user-facing CRDs

See [SDK doc to generate CSV for OLM](https://github.com/operator-framework/operator-sdk/blob/6c8f101c3b0880e77d481c990a93ff2491ae20a2/doc/user/olm-catalog/olm-cli.md) and [OLM doc](https://github.com/operator-framework/olm-book/)

```
operator-sdk generate csv --csv-version 0.0.1
```

The result lands in `deploy\olm-catalog\webeossitemgmt-operator\0.0.1\webeossitemgmt-operator.v0.0.1.clusterserviceversion.yaml`

Because this is an Ansible operator, we need to define the resource schema and fields manually in the CSV
(and CRD as well): it cannot be generated automatically from a Go type. Here's a list of manual changes done:

* Remove `replaces: webeossitemgmt-operator.v0.0.0` (no such version exists)
* Set required fields: `spec.maintainers.name: Required value, spec.maintainers.email: Required value, spec.icon.mediatype: Unsupported value: "": supported values: "image/gif", "image/jpeg", "image/png", "image/svg+xml"` (error seen in catalog-operator pod logs in openshift-operator-lifecycle-manager namespace)
* Remove deployment and permissions, since we'll deploy the operator with Helm/ArgoCD
* Provide the UI definition:
   *  define CRD fields under `spec.customresourcedefinitions.owned` as per [SDK doc](https://docs.openshift.com/container-platform/4.1/applications/operator_sdk/osdk-generating-csvs.html#osdk-crds_osdk-generating-csvs)
   * See [reference](https://github.com/openshift/console/blob/e9c7873cf3a76245b6defeb6b4a708d46f451cc2/frontend/packages/operator-lifecycle-manager/src/components/descriptors/reference/reference.md)
   * [Advanced example](https://github.com/ParFlesh/sonarr-operator/blob/6174a4df2ff77d045106c1e6300be5a0898bb6b8/pkg/apis/sonarr/v1alpha1/sonarr_types.go)

Copy the CRD YAML file from `deploy\crds\webeos.webservices.cern.ch_userprovideddirectories_crd.yaml`
into the operator's helm chart.

This CSV resource tells OLM how to publish our operator. See OLM doc for details: https://github.com/operator-framework/olm-book/

Usually operators managed by OLM are deployed via the OLM catalog. But we skip the catalog because:
1. we are only interested in the UI part, and it requires a CSV. For the webeos use case, other OLM features are not useful.
2. this operator is part of a large set of components to support webeos, and we prefer using the same
    methods to deploy and upgrade this operator as the rest (i.e. using ArgoCD and Helm charts)

### How OLM deploys the CSV to each namespace

When our CSV gets created in the cluster, the olm-operator will look for an `OperatorGroup` resource _in the same namespace_
and _copy_ the CSV to each target namespace (all namespaces in this case).
The operator then shows up in the catalog as "Installed operators" in the Openshift console of the projects where the CSV
was copied to. And users can create CRs with a UI generated from this CSV's `spec.customresourcedefinitions.owned.specDescriptors`

**IMPORTANT NOTE:** each CSV resource is only ever copied once to each destination project. If we make changes to it,
the change will NOT be propagated to projects that have a copy of a previous version.
We need to create a new CSV for every change, that replaces the previous one.

We make this happen by generating a new CSV resource name with each Helm release. This cause Helm to
delete the old CSV and create a new one with a new name. The OLM then deletes the old CSV and creates
the new one in each namespace.

### Notes about using the OLM catalog

We don't use the OLM catalog in the end, but notes about it can be found
[in Notes of the `using-olm-catalog` branch](https://gitlab.cern.ch/paas-tools/operators/webeossitemgmt-operator/-/blob/using-olm-catalog/NOTES.md)